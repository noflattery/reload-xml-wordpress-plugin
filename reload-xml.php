<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.nathanalexanderthompson.com
 * @since             1.0.0
 * @package           Reload_Xml
 *
 * @wordpress-plugin
 * Plugin Name:       Reload XML
 * Plugin URI:        https://bitbucket.org/noflattery/reload-xml-wordpress-plugin
 * Description:       Pulls in data from xml file and hooks into Manual Rates settings.
 * Version:           1.0.0
 * Author:            Nathan Thompson
 * Author URI:        http://www.nathanalexanderthompson.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       reload-xml
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-reload-xml-activator.php
 */
function activate_reload_xml() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-reload-xml-activator.php';
	Reload_Xml_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-reload-xml-deactivator.php
 */
function deactivate_reload_xml() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-reload-xml-deactivator.php';
	Reload_Xml_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_reload_xml' );
register_deactivation_hook( __FILE__, 'deactivate_reload_xml' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-reload-xml.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_reload_xml() {

	$plugin = new Reload_Xml();
	$plugin->run();

}
run_reload_xml();
