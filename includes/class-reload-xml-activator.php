<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.nathanalexanderthompson.com
 * @since      1.0.0
 *
 * @package    Reload_Xml
 * @subpackage Reload_Xml/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Reload_Xml
 * @subpackage Reload_Xml/includes
 * @author     Nathan Thompson <nate.a.thompson@gmail.com>
 */
class Reload_Xml_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
