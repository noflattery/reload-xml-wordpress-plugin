<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.nathanalexanderthompson.com
 * @since      1.0.0
 *
 * @package    Reload_Xml
 * @subpackage Reload_Xml/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Reload_Xml
 * @subpackage Reload_Xml/includes
 * @author     Nathan Thompson <nate.a.thompson@gmail.com>
 */
class Reload_Xml_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
