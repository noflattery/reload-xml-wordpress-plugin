<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.nathanalexanderthompson.com
 * @since      1.0.0
 *
 * @package    Reload_Xml
 * @subpackage Reload_Xml/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
